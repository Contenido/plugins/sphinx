create sequence search_id_seq;
select setval('search_id_seq', 1, true);

create table search
(
	id integer not null primary key default nextval('public.search_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	status smallint not null default 1,
	is_deleted boolean default false,
	object_id integer not null,
	object_class text not null,
	name text,
	search text,
	data text,
	UNIQUE(object_id, object_class)
);
create UNIQUE index search_object on search (object_class, object_id);
create index search_mtime on search (mtime);
