package sphinx::Search;

use base 'Contenido::Document';
use Contenido::Globals;

sub extra_properties
{
   return (
	)
}


sub sections
{
	return ();
}

sub class_name
{
	return 'Sphinx: поисковый объект';
}

sub class_description
{
	return 'Sphinx: поисковый объект';
}

sub class_table
{
	return 'sphinx::SQL::SearchTable';
}

sub search_fields {
	return ('name', 'object_id');
}

sub pre_store
{
	my $self = shift;

	return 1;
}


1;
