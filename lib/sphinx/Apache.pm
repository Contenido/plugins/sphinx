package sphinx::Apache;

use strict;
use warnings 'all';

use sphinx::State;
use Contenido::Globals;


sub child_init {
	# встраиваем keeper плагина в keeper проекта
	$keeper->{sphinx} = sphinx::Keeper->new($state->sphinx);
}

sub request_init {
}

sub child_exit {
}

1;
