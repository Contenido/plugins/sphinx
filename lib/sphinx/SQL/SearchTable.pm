package sphinx::SQL::SearchTable;

use base 'SQL::ProtoTable';

sub db_table
{
	return 'search';
}

sub db_id_sequence {
	return 'search_id_seq';
}

sub available_filters {
	my @available_filters = qw(	
					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
					_class_excludes_filter

					_object_id_filter
					_object_class_filter
					_excludes_filter
				);
	return \@available_filters; 
}

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда 
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
	return (
		{							# Идентификатор документа, сквозной по всем типам...
			'attr'		=> 'id',
			'type'		=> 'integer',
			'rusname'	=> 'Идентификатор документа',
			'hidden'	=> 1,
			'auto'		=> 1,
			'readonly'	=> 1,
			'db_field'	=> 'id',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null default nextval('public.documents_id_seq'::text)", 
		},
		{							# Класс документа...
			'attr'		=> 'class',
			'type'		=> 'string',
			'rusname'	=> 'Класс документа',
			'hidden'	=> 1,
			'readonly'	=> 1,
			'column'	=> 3,
			'db_field'	=> 'class',
			'db_type'	=> 'varchar(48)',
			'db_opts'	=> 'not null',
		},
		{
			'attr'		=> 'name',
			'type'		=> 'string',
			'rusname'	=> 'Название документа',
			'column'	=> 2,
			'db_field'	=> 'name',
			'db_type'       => 'text',
		},
		{
			'attr'		=> 'status',
			'type'		=> 'status',
			'rusname'       => 'Статус',
			'db_field'      => 'status',
			'db_type'       => 'smallint',
		},
		{
			'attr'		=> 'is_deleted',
			'type'		=> 'checkbox',
			'rusname'       => 'Документ удален',
			'db_field'      => 'is_deleted',
			'db_type'       => 'boolean',
		},
		{							# Время создания документа
			'attr'		=> 'ctime',
			'type'		=> 'datetime',
			'rusname'	=> 'Дата/время создания',
			'readonly'	=> 1,
			'auto'          => 1,
			'hidden'	=> 1,
			'db_field'	=> 'ctime',
			'db_type'	=> 'timestamp',
			'db_opts'	=> 'not null default now()',
			'default'	=> 'CURRENT_TIMESTAMP',
		},
		{							# Время изменения документа
			'attr'		=> 'mtime',
			'type'		=> 'datetime',
			'rusname'	=> 'Дата/время изменения',
			'auto'          => 1,
			'hidden'	=> 1,
			'db_field'	=> 'mtime',
			'db_type'	=> 'timestamp',
			'db_opts'	=> 'not null default now()',
			'default'	=> 'CURRENT_TIMESTAMP',
		},
		{
			'attr'		=> 'object_class',
			'type'		=> 'string',
			'rusname'	=> 'Класс объекта',
			'column'	=> 4,
			'db_field'	=> 'object_class',
			'db_type'       => 'text',
		},
		{
			'attr'		=> 'object_id',
			'type'		=> 'string',
			'rusname'	=> 'ID объекта',
			'column'	=> 5,
			'db_field'	=> 'object_id',
			'db_type'       => 'integer',
		},
		{
			'attr'          => 'search',
			'type'          => 'text',
			'rusname'       => 'Текст',
			'db_field'      => 'search',
			'db_type'       => 'text',
		},
	);
}


########### FILTERS DESCRIPTION ####################################################################################
sub _excludes_filter {
	my ($self,%opts)=@_;
	if (exists $opts{excludes}) {
		# - исключение из отбора
		my @eids = ();
		if (ref($opts{excludes}) eq 'ARRAY') {
			@eids = @{ $opts{excludes} };
		} elsif ($opts{excludes} =~ /[^\d\,]/) {
			warn "Contenido Warning: В списке идентификаторов для исключения встречаются нечисловые элементы. Параметр excludes игнорируется (".$opts{excludes}.").\n";
		} else {
			@eids = split(',', $opts{excludes});
		}

		my $excludes = join(',', @eids);

		# Меняется логика запроса, если это join-запрос.
		# Потому что в этом случае гораздо лучше ограничить выборку по таблице links,
		#  чем производить полную склейку таблиц.
		if (@eids) {
			if (exists($opts{ldest}) || exists($opts{lsource})) {
				if (exists($opts{ldest})) {
					return " (l.source_id not in ($excludes)) ";
				} elsif (exists($opts{lsource})) {
					return " (l.dest_id   not in ($excludes)) ";
				}
			} else {
				return  " (d.id not in ($excludes)) ";
			}
		}
	}
	return undef;
}


sub _get_orders {
	my ($self, %opts) = @_;

	if ($opts{order_by}) {
		return ' order by '.$opts{order_by};
	} else {
		return ' order by mtime desc';
	}
	return undef;
}

sub _object_id_filter {
	my ($self, %opts)=@_;
	return undef unless ( exists $opts{object_id}  );
	return &SQL::Common::_generic_int_filter('d.object_id', $opts{object_id});
}

sub _object_class_filter {
	my ($self, %opts)=@_;
	return undef unless ( exists $opts{object_class}  );
	return &SQL::Common::_generic_text_filter('d.object_class', $opts{object_class});
}

1;
