package sphinx::Init;

use strict;
use warnings 'all';

use Contenido::Globals;
use sphinx::Apache;
use sphinx::Keeper;
use sphinx::Search;
use sphinx::SQL::SearchTable;

# загрузка всех необходимых плагину классов
# sphinx::SQL::SomeTable
# sphinx::SomeClass
Contenido::Init::load_classes(qw(
		sphinx::Search
	));

sub init {
	0;
}

1;
